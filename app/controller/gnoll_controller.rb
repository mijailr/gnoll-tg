class GnollController < ApplicationController
rescue_from Exception, with: :render_200 # Responde con 200 siempre, auque exista un error en la transaccion-
def random(gnoll)
  mensajes = ['Aaarghhh!!!', 'Braaiiinnzzz..', 'Grmbblrr..', 'GRRRRRR...!!', 'Bluuughhrr..']
  gnoll.responde({chat_id: gnoll.telegram.message.chat.id, text: mensajes.sample, reply_to_message_id: gnoll.telegram.messa$
end
def sonando(gnoll)
  radio_gnu = JSON.parse RestClient.get("http://api.radiognu.org/?img_size=200")

  iconos = {
    radio: "\xF0\x9F\x93\xBB".force_encoding(Encoding::UTF_8),
    pista: "\xF0\x9F\x8E\xB6".force_encoding(Encoding::UTF_8),
    artista: "\xF0\x9F\x91\xA4".force_encoding(Encoding::UTF_8),
    disco: "\xF0\x9F\x92\xBF".force_encoding(Encoding::UTF_8),
    licencia: "\xF0\x9F\x93\x83".force_encoding(Encoding::UTF_8),
    escuchas: "\xF0\x9F\x8E\xA7".force_encoding(Encoding::UTF_8),
    microfono: "\xF0\x9F\x8E\xA4".force_encoding(Encoding::UTF_8)
  }

  if radio_gnu['isLive']
    broadcast= "#{iconos[:microfono]} «#{radio_gnu['show']}» de #{radio_gnu['broadcaster']}\n"
    status = "#ENVIVO"
    licencia = ""
  else
    broadcast = ""
    status = "#DIFERIDO"
    licencia = "" #"#{iconos[:licencia]} #{radio_gnu['licence']['shortname']}\n"
  end
  mensaje = "#{broadcast}#{iconos[:pista]} #{radio_gnu['title']}\n#{iconos[:artista]} #{radio_gnu['artist']}\n#{licencia}#{iconos[:escuchas]} #{radio_gnu['listeners']} escuchas\n#{iconos[:radio]} #{status}"
  arte_album = Tempfile.new(["radio_gnu", ".png"])
  datos_imagen = Base64.decode64(radio_gnu['cover']['data:image/png;base64,'.length .. -1]).force_encoding('UTF-8').encode
  arte_album.write(datos_imagen)
  arte_album.rewind
  gnoll.imagen({ photo: arte_album, chat_id: gnoll.telegram.message.chat.id, caption: mensaje })
end
def gnoll_bot
  gnoll = TgBot.new({ bot_key: "--------" })
  gnoll.escucha(params)
  case gnoll.comando
  when '/sonando'
    sonando(gnoll) and return
  else
       random(gnoll) and return
     end
   end
   private
   def render_200
     render json: "{}"
   end
 end
